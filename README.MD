# Contoh Aplikasi Perpustakaan dengan Konsep OOP dan Java

Aplikasi perpustakaan ini dibangun dengan menggunakan konsep OOP dan bahasa pemrograman Java.

## Kelas-kelas yang Diperlukan

Aplikasi ini menggunakan empat kelas yang terdiri dari:

1. Buku
2. Anggota
3. Peminjaman
4. Perpustakaan (Kelas Utama)

Setiap kelas memiliki peran yang berbeda-beda dalam aplikasi dan saling terkait satu sama lain. 

Silahkan melihat source code untuk melihat detail implementasi dari setiap kelas.

## Cara Menjalankan Aplikasi

Untuk menjalankan aplikasi, pastikan Anda memiliki environment Java yang sudah terinstall di komputer Anda. Kemudian, jalankan file Perpusatakaan.java pada direktori utama aplikasi.


